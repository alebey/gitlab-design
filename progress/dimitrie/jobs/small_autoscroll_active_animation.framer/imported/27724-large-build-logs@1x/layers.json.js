window.__imported__ = window.__imported__ || {};
window.__imported__["27724-large-build-logs@1x/layers.json.js"] = [
	{
		"objectId": "98B7AADF-E9E9-41A7-8F79-885A80FFDE3A",
		"kind": "artboard",
		"name": "top",
		"originalName": "top",
		"maskFrame": null,
		"layerFrame": {
			"x": 510,
			"y": 1115,
			"width": 70,
			"height": 64
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "0DAB6837-7965-4A10-98E4-E29085561AEC",
				"kind": "group",
				"name": "Group_2_Copy",
				"originalName": "Group 2 Copy",
				"maskFrame": null,
				"layerFrame": {
					"x": 29,
					"y": 22,
					"width": 12,
					"height": 16
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Group_2_Copy-merbqjy4.png",
					"frame": {
						"x": 29,
						"y": 22,
						"width": 12,
						"height": 16
					}
				},
				"children": []
			},
			{
				"objectId": "61AEE713-147C-455D-A693-3FE9406995FA",
				"kind": "group",
				"name": "System_Information",
				"originalName": "System Information",
				"maskFrame": null,
				"layerFrame": {
					"x": -908,
					"y": -471,
					"width": 961,
					"height": 520
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "A58FF164-D5AA-4D33-82D3-1A02F04D452A",
						"kind": "group",
						"name": "Rectangle_14",
						"originalName": "Rectangle 14",
						"maskFrame": {
							"x": 0,
							"y": 0,
							"width": 959,
							"height": 517.822445561139
						},
						"layerFrame": {
							"x": -908,
							"y": -471,
							"width": 961,
							"height": 520
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_14-qtu4rkyx.png",
							"frame": {
								"x": -908,
								"y": -471,
								"width": 961,
								"height": 520
							}
						},
						"children": [
							{
								"objectId": "7C02BA33-04B8-4119-8BBA-55945FD35B1F",
								"kind": "group",
								"name": "Group_10",
								"originalName": "Group 10",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 12,
									"width": 37,
									"height": 37
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_10-n0mwmkjb.png",
									"frame": {
										"x": 16,
										"y": 12,
										"width": 37,
										"height": 37
									}
								},
								"children": []
							}
						]
					}
				]
			}
		]
	}
]